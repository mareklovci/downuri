beautifulsoup4==4.6.1
certifi==2019.3.9
Click==7.0
# downuri==0.1.0
heapdict==1.0.0
lxml==4.2.5
mkl_fft==1.0.10
mkl_random==1.0.2
numpy==1.16.2
pandas==0.24.2
python-dateutil==2.8.0
pytz==2018.9
six==1.12.0
streamz==0.5.0
toolz==0.9.0
tornado==6.0.2
wincertstore==0.2
zict==0.1.4
