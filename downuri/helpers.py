def _csv_list_to_dataframe(csvs: list):
    list_ = []
    for csv in csvs:
        data = Path(join(sets.DATA_FOLDER, csv))
        df = pd.read_csv(data, ',', error_bad_lines=False)
        list_.append(df)
    return pd.concat(list_, axis=0, join='outer', ignore_index=True)


def _generate_files(df_bad, df_good):
    size: int = 10000
    for i in range(1, 10):  # 1..9
        # e.g. data_90_10.csv
        name: str = 'data_' + str(i * 10) + '_' + str((10 - i) * 10) + '.csv'
        df1 = df_bad.sample(i * size)  # get random n rows
        df2 = df_good.sample((10 - i) * size)
        df = pd.concat([df1, df2])
        df.to_csv(Path(join(sets.DATA_FOLDER, 'partials', name)), sep=',', encoding='utf-8', index=False)


def _process_file_generating():
    df = _csv_list_to_dataframe(['data.csv', 'urls10000.csv', 'urls4357.csv'])

    # without header -> just values
    print("Number of values in dataset: {} ({} %)".format(len(df.index), len(df.index) / len(df.index) * 100))
    labels = df.label.value_counts()

    good_percentage = int(round(labels.good / len(df.index) * 100))
    bad_percentage = int(round(labels.bad / len(df.index) * 100))

    print("Number of good values: {} ({} %)".format(labels.good, good_percentage))
    print("Number of bad values: {} ({} %)".format(labels.bad, bad_percentage))

    df1: pd.DataFrame = df[df['label'] == 'bad']  # NOTE(01-11-2018): len(df.index) = 90000
    df2: pd.DataFrame = df[df['label'] == 'good']  # NOTE(01-11-2018): len(df.index) = 344821

    _generate_files(df1, df2)
