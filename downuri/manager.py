#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import csv
import random
import re
import statistics
import downuri.sets as sets

_url_regex = r'[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)'


def iter_sample(iterable, sample_size) -> list:
    """Get random n of items from generator

    :param iterable: iterable to get samples from
    :param sample_size: number of items (n)
    :return: list of len(n) with random elements from iterable
    """
    results = []
    iterator = iter(iterable)
    # Fill in the first sample_size elements:
    try:
        for _ in range(sample_size):
            results.append(next(iterator))
    except StopIteration:
        raise ValueError("Sample larger than population.")
    random.shuffle(results)  # Randomize their positions
    for i, v in enumerate(iterator, sample_size):
        r = random.randint(0, i)
        if r < sample_size:
            results[r] = v  # at a decreasing rate, replace random items
    return results


def read_kaggle(malicious=None, filename=sets.DATA_FOLDER_VER2 / sets.DATA_MIXED):
    with open(filename, 'rt', encoding='utf8') as csvfile:
        datareader = csv.reader(csvfile, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True)
        # yield next(datareader)  # yield the header row
        for row in datareader:
            if not re.match(_url_regex, row[0]):
                continue
            elif malicious and row[1] == 'bad':
                yield row[0]
            elif not malicious and row[1] == 'good':
                yield row[0]
            elif malicious is None:
                yield row[0]


def read_hosts(filename=sets.DATA_FOLDER_VER2 / sets.UNIFIED_HOSTS):
    with open(filename, 'rt', encoding='utf8') as csvfile:
        datareader = csv.reader(csvfile)
        for row in datareader:
            if row and row[0].startswith('0.0.0.0'):
                uri = row[0].strip().split()[1]
                if not re.match(_url_regex, uri):
                    continue
                else:
                    yield uri


def _average(items):
    total_sum = 0
    for item in items:
        total_sum += len(item)
    return round(total_sum / len(items))


def _modus(items):
    len_dict = {}
    for item in items:
        item_len = len(item)
        if item_len in len_dict:
            len_dict[item_len] += 1
        else:
            len_dict[item_len] = 1
    return max(len_dict, key=len_dict.get)


def _median(items):
    num_list = [len(x) for x in items]
    return statistics.median(num_list)


def _read_combined(filename=sets.DATA_FOLDER_VER2 / 'explainer_set.csv'):
    with open(filename, 'rt', encoding='utf8') as csvfile:
        datareader = csv.reader(csvfile, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True)
        for row in datareader:
            yield row[0]


def read_all_and_save_to_file():
    data = {'kaggle_mal': read_kaggle(malicious=True),
            'kaggle_ben': read_kaggle(malicious=False),
            'hosts': read_hosts()}

    with open('urls.csv', 'w', encoding='utf8') as f:
        f.write('{},{}\n'.format('url', 'label'))
        for key, items in data.items():
            for item in items:
                if key == 'kaggle_mal':
                    f.write('{url},{label}\n'.format(url=item, label='bad'))
                if key == 'kaggle_ben':
                    f.write('{url},{label}\n'.format(url=item, label='good'))
                if key == 'hosts':
                    f.write('{url},{label}\n'.format(url=item, label='bad'))


def split(filehandler, delimiter=',', row_limit=20000, output_name_template='urls_%s.csv', output_path='.',
          keep_headers=True):
    """
    Splits a CSV file into multiple pieces.

    A quick bastardization of the Python CSV library.
    Arguments:
        `row_limit`: The number of rows you want in each output file. 10,000 by default.
        `output_name_template`: A %s-style template for the numbered output files.
        `output_path`: Where to stick the output files.
        `keep_headers`: Whether or not to print the headers in each output file.
    Example usage:

        >> from toolbox import csv_splitter;
        >> csv_splitter.split(open('/home/ben/input.csv', 'r'));

    """
    import csv
    reader = csv.reader(filehandler, delimiter=delimiter)
    current_piece = 1
    current_out_path = os.path.join(
        output_path,
        output_name_template % current_piece
    )
    current_out_writer = csv.writer(open(current_out_path, 'w', encoding='utf8', newline=''), delimiter=delimiter)
    current_limit = row_limit
    if keep_headers:
        headers = next(reader)
        current_out_writer.writerow(headers)
    for i, row in enumerate(reader):
        if i + 1 > current_limit:
            current_piece += 1
            current_limit = row_limit * current_piece
            current_out_path = os.path.join(
                output_path,
                output_name_template % current_piece
            )
            current_out_writer = csv.writer(open(current_out_path, 'w', encoding='utf8', newline=''), delimiter=delimiter)
            if keep_headers:
                current_out_writer.writerow(headers)
        current_out_writer.writerow(row)


def statistic():
    data = {'kaggle': read_kaggle(),
            'hosts': read_hosts(),
            'explainer': _read_combined()}

    for key, value in data.items():
        print(key)

        dat = list(value)

        minimal = min((len(x) for x in dat))
        maximal = max((len(x) for x in dat))
        average = _average(dat)
        modus = _modus(dat)
        median = _median(dat)

        print('minimal url length: {}'.format(minimal))
        print('average url length: {}'.format(average))
        print('modus url length: {}'.format(modus))
        print('median url length: {}'.format(median))
        print('maximum url length: {}'.format(maximal))


def main():
    for url in read_kaggle(malicious=True):
        print(url)

    for url in read_hosts():
        print(url)

    for url in read_kaggle(malicious=False):
        print(url)

    len_hosts = len(list(read_hosts()))
    print('Hosts: {hosts}'.format(hosts=len_hosts))

    len_kaggle_bad = len(list(read_kaggle(malicious=True)))
    print('Kaggle_bad: {kaggle_bad}'.format(kaggle_bad=len_kaggle_bad))

    len_kaggle_good = len(list(read_kaggle(malicious=False)))
    print('Kaggle_good: {kaggle_good}'.format(kaggle_good=len_kaggle_good))


if __name__ == '__main__':
    # read_all_and_save_to_file()
    split(open('urls.csv', 'r', encoding='utf8', newline=''))
