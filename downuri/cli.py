#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from itertools import chain
import downuri.sets as sets

import click

from downuri.manager import iter_sample, read_hosts, read_kaggle

logger = logging.getLogger(__name__)


class AliasedGroup(click.Group):

    def get_command(self, ctx, cmd_name):
        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [x for x in self.list_commands(ctx)
                   if x.startswith(cmd_name)]
        if not matches:
            return None
        elif len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail('Too many matches: %s' % ', '.join(sorted(matches)))


@click.group(cls=AliasedGroup, chain=False)
@click.option('--log-level', '-l', default='info', type=click.Choice(['debug', 'info', 'warning', 'error', 'critical']))
def downuri(log_level):
    """Text with explanation what the heck is this CLI going to be about
    """
    logging.basicConfig(level=getattr(logging, log_level.upper()))


@downuri.command(name='genurls', short_help='Returns URLs')
@click.option('--bad', '-b', required=True, type=int)
@click.option('--good', '-g', required=True, type=int)
def genurls(bad, good):
    # get N malicious
    combined_generator = chain(
        read_kaggle(filename=sets.DATA_FOLDER / sets.DATA_MIXED, malicious=True),
        read_hosts(filename=sets.DATA_FOLDER / sets.UNIFIED_HOSTS))
    malicious = iter_sample(combined_generator, bad)

    # get M malign
    malign = iter_sample(read_kaggle(filename=sets.DATA_FOLDER / sets.DATA_MIXED, malicious=False), good)

    for url in malicious:
        click.echo('{url},bad'.format(url=url).encode("utf-8"))

    for url in malign:
        click.echo('{url},good'.format(url=url).encode("utf-8"))


@downuri.command(name='getsize', short_help='Returns number of available elements')
@click.option('--url', '-u', required=True, type=click.Choice(['good', 'bad'], case_sensitive=False))
def getsize(url):
    list_length = None
    if url == 'bad':
        chained = chain(
            read_kaggle(filename=sets.DATA_FOLDER / sets.DATA_MIXED, malicious=True),
            read_hosts(filename=sets.DATA_FOLDER / sets.UNIFIED_HOSTS))
        list_length = len(list(chained))
    elif url == 'good':
        list_length = len(list(read_kaggle(filename=sets.DATA_FOLDER / sets.DATA_MIXED, malicious=False)))
    click.echo(list_length)


@downuri.command(name='datapath', short_help='Returns path where downloaded datasets should be located')
def datapath():
    click.echo(sets.DATA_FOLDER)


if __name__ == '__main__':
    downuri()
