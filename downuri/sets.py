#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

# Path to data folder
DATA_FOLDER = Path('.').absolute() / 'data'

DATA_FOLDER_VER2 = Path('.').absolute().parent / 'data'

# Kaggle data
DATA_MIXED = 'data.csv'

# Unified hosts = (adware + malware)
UNIFIED_HOSTS = 'hosts.txt'

if __name__ == '__main__':
    pass
