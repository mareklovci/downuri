#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(name='downuri',
      version='0.1.0',
      packages=['downuri'],
      entry_points={
          'console_scripts': [
              'downuri = downuri.cli:downuri',
              'downurl = downuri.cli:downuri'
          ]
      }, install_requires=['click']
      )
