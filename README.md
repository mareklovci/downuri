# downuri

downURI CLI tool serves to downloading files with URL addresses and for creating csv datasets.

## Data size
* good URLs: **344,821**
* malicious URLs: **113,966**

## Install

Use `python setup.py install --user` for install.

## Anaconda

Use `conda env export > environment.yml` to export conda environment
