import unittest
from pathlib import Path

import downuri.manager as manager
import downuri.sets as sets


class TestHostsFileProcessing(unittest.TestCase):

    def setUp(self):
        self.DATA_FOLDER = Path('.').absolute() / 'data'
        self.HOSTS_FILE_PATH = self.DATA_FOLDER / sets.UNIFIED_HOSTS
        self.result = ['1493361689.rsc.cdn77.org', '30-day-change.com', '2468.go2cloud.org']

    def test_data_retrieve(self):
        urls = list(manager.read_hosts(filename=self.HOSTS_FILE_PATH))
        self.assertListEqual(urls, self.result, "Lists should be identical.")

    def test_bad_data_length(self):
        self.assertCountEqual(manager.read_hosts(filename=self.HOSTS_FILE_PATH), self.result,
                              "Lists should have same length")


if __name__ == '__main__':
    unittest.main()
